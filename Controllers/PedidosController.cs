using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PedidosController: ControllerBase
    {
        private readonly VendaContext _context;
        public PedidosController(VendaContext context)
        {
            _context = context;
        }
        [HttpPost]
        public IActionResult RegistrarVenda(Pedido pedido)
        {
            _context.Add(pedido);
            _context.SaveChanges();
            
            return CreatedAtAction(nameof(ObterPorId), new { id = pedido.Id }, pedido);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var pedido = _context.Pedidos.Find(id);

            if (pedido == null)
                return NotFound();

            return Ok(pedido);
        }
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Pedido pedido)
        {
            var pedidoBanco = _context.Pedidos.Find(id);

            if (pedidoBanco == null)
                return NotFound();

            pedidoBanco.Produto = pedido.Produto;
            pedidoBanco.Status = pedido.Status;


            _context.Pedidos.Update(pedidoBanco);
            _context.SaveChanges();

            return Ok(pedidoBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var pedidoBanco = _context.Pedidos.Find(id);

            if (pedidoBanco == null)
                return NotFound();

            _context.Pedidos.Remove(pedidoBanco);
            _context.SaveChanges();
            return NoContent();
        }
    }
}